import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;
import 'package:sqflite/sqlite_api.dart';

// Functionality to interact with the database
class DBHelper {
  static Future<Database> database() async {
    // To write to a database we need access to it
    // first create database if we do not have one
    // dpPath is a folder where to store the db
    final dbPath = await sql.getDatabasesPath();
    // either finds a database in the path and opens it or creates a new one places.db
    // in the dbPath
    return sql.openDatabase(path.join(dbPath, 'places.db'),
        onCreate: (db, version) {
          // image is the path to the image on the device
          return db.execute(
            // Field for the location data REAL stores double
              'CREATE TABLE user_places(id TEXT PRIMARY KEY, title TEXT, image TEXT, '
                  'loc_lat REAL, loc_lng REAL, address TEXT)');
        }, version: 1);
  }
  // table is the table in the database that this should be added to
  static Future<void> insert(String table, Map<String, Object> data) async {
      final db = await DBHelper.database();
      db.insert(
      table, data,
      // If we try to insert data for an existing id, the entry will be overridden  with the new data
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<List<Map<String, dynamic>>> getData(String table) async {
    final db = await DBHelper.database();
    // Return everything from the table
    return db.query(table);
  }
}




















import 'dart:convert';

import 'package:http/http.dart' as http;
// api AIzaSyBBQDOqVEHYi-kB5gxM5FbPfVDI6IZ5X9E
/*
https://developers.google.com/maps/documentation/maps-static/overview

https://maps.googleapis.com/maps/api/staticmap?center=Brooklyn+Bridge,New+York,NY&zoom=13&size=600x300&maptype=roadmap
&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318
&markers=color:red%7Clabel:C%7C40.718217,-73.998284
&key=YOUR_API_KEY
 */

const GOOGLE_API_KEY = 'AIzaSyBBQDOqVEHYi-kB5gxM5FbPfVDI6IZ5X9E';

class LocationHelper {
  static String generateLocationPreviewImage({double latitude, double longitude}){
    // Replace the center address with latitude and longitude
    return 'https://maps.googleapis.com/maps/api/staticmap?center=&$latitude,$longitude&zoom=16&size=600x300&maptype=roadmap&markers=color:red%7Clabel:A%7C$latitude,$longitude&key=$GOOGLE_API_KEY';
  }

  // Returns a string address of the place on that lat and lng
  static Future<String> getPlaceAddress(double lat, double lng) async {
    final url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&key=$GOOGLE_API_KEY';
    // Send a get request to google server, returns a response JSON
    final response = await http.get(url);
    // response.body give a map
    //{
    // plus_code: {
    // compound_code: "P27Q+MC Ню Йорк, Съединени щати",
    // global_code: "87G8P27Q+MC"
    // },
    // results: [
    // {
    // address_components: [
    // {
    // long_name: "277",
    // short_name: "277",
    // types: [
    // "street_number"
    // ]
    // },
    //formatted_address: "277 Bedford Ave, Brooklyn, NY 11211, Съединени щати",
    return json.decode(response.body)['results'][0]['formatted_address'];
  }
}
// build static snapshot image

















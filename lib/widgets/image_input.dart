import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspaths;

class ImageInput extends StatefulWidget {
  // A pointer to a function we expect to get from outside
  final Function onSelectImage;

  ImageInput(this.onSelectImage);

  @override
  _ImageInputState createState() => _ImageInputState();
}

class _ImageInputState extends State<ImageInput> {
  File _storedImage;

  Future<void>_takePicture() async {
    // Opens the camera and picks an image, after taking a photo, it is stored
    // in the imageFile
    final imageFile = await ImagePicker.pickImage(
      source: ImageSource.camera,
      maxWidth: 600,
    );
    if(imageFile == null){
      return;
    }
    setState(() {
      _storedImage = imageFile;
    });
    // The directory into which the image copy will be saved
    final appDir = await syspaths.getApplicationDocumentsDirectory();
    // Get the name of the image file including file extension
    final fileName = path.basename(imageFile.path);
    //Copy the file imageFile into path appDir.path and keep the file name fileName
    // The image that is saved at its final destination
    // The / is to navigate into the path
    final savedImage = await imageFile.copy('${appDir.path}/$fileName');
    // widget global property available in state objects giving access to the
    // widget class ImageInput
    // The AddPlaceScreenState receives the image
    widget.onSelectImage(savedImage);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: 150,
          height: 100,
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.grey),
          ),
          child: _storedImage != null
              // render image preview from a file on the device
              ? Image.file(
                  _storedImage,
                  fit: BoxFit.cover,
                  width: double.infinity,
                )
              : Text('No Image Take', textAlign: TextAlign.center),
          alignment: Alignment.center,
        ),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: FlatButton.icon(
            icon: Icon(Icons.camera),
            label: Text('Take Picture'),
            textColor: Theme.of(context).primaryColor,
            onPressed: _takePicture,
          ),
        ),
      ],
    );
  }
}

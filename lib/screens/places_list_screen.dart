import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './add_place_screen.dart';
import '../providers/great_places.dart';
import './place_detail_screen.dart';

// Used as home screen of the app
class PlacesListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Places'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).pushNamed(AddPlaceScreen.routeName);
            },
          ),
        ],
      ),
      body: FutureBuilder(
        future: Provider.of<GreatPlaces>(context, listen: false)
            .fetchAndSetPlaces(),
        //snapshot of the result the future returns
        builder: (ctx, snapshot) =>
            //If we are waiting for the result
            snapshot.connectionState == ConnectionState.waiting
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Consumer<GreatPlaces>(
                    // The child will not update even if the data changes
                    child: Center(
                      child:
                          const Text('Got no places yet, start adding some!'),
                    ),
                    // ch is the child from above Text('Got no places yet, start adding some!'),
                    builder: (ctx, greatPlaces, ch) =>
                        greatPlaces.items.length <= 0
                            ? ch
                            : ListView.builder(
                                itemCount: greatPlaces.items.length,
                                // Return a ListTile for every place
                                itemBuilder: (ctx, i) => ListTile(
                                  leading: CircleAvatar(
                                    backgroundImage: FileImage(
                                      greatPlaces.items[i].image,
                                    ),
                                  ),
                                  title: Text(greatPlaces.items[i].title),
                                  subtitle: Text(
                                      greatPlaces.items[i].location.address),
                                  onTap: () {
                                    Navigator.of(context).pushNamed(
                                      PlaceDetailScreen.routeName,
                                      // Pass only the id, because we try to
                                      // retrieve it in the  PlaceDetailScreen
                                      arguments: greatPlaces.items[i].id,
                                    );
                                  },
                                ),
                              ),
                  ),
      ),
    );
  }
}

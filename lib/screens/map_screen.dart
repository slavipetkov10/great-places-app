import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../models/place.dart';

class MapScreen extends StatefulWidget {
  final PlaceLocation initialLocation;

  // Whether I want to let the user tap on the map and select a new place
  // or it is a read-only map
  final bool isSelecting;

  // if the initialLocation is not set from outside the default value
  // is used
  MapScreen(
      {this.initialLocation =
          const PlaceLocation(latitude: 37.422, longitude: -122.084),
      this.isSelecting = false});

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  LatLng _pickedLocation;

  // LatLng gives the position of the tap automatically
  void _selectLocation(LatLng position) {
    // Add a visible marker
    // setState reruns the bellow build method
    setState(() {
      _pickedLocation = position;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Map'),
        actions: <Widget>[
          // If we are in selecting mode and are able to select a place,
          // the button will be added to the list
          if (widget.isSelecting)
            IconButton(
                icon: Icon(Icons.check),
                // The button is disabled if not location is picked
                onPressed: _pickedLocation == null
                    ? null
                    : () {
                        // Pops off the screen
                        Navigator.of(context).pop(
                            // The data that is returned back to the previous screen
                            // location input
                            _pickedLocation);
                      }),
        ],
      ),
      body: GoogleMap(
        initialCameraPosition: CameraPosition(
          target: LatLng(
            widget.initialLocation.latitude,
            widget.initialLocation.longitude,
          ),
          zoom: 16,
        ),
        onTap: widget.isSelecting ? _selectLocation : null,
        // After we tap the screen the marker is displayed since setState
        // calls this build method
        // If we are not selecting I want to show a marker
        markers: (_pickedLocation == null && widget.isSelecting)
            ? null
            : {
                Marker(
                  markerId: MarkerId('m1'),
                  // If pickedLocation is null then fall back to initial location
                  position: _pickedLocation ??
                      LatLng(
                        widget.initialLocation.latitude,
                        widget.initialLocation.longitude,
                      ),
                ),
              },
      ),
    );
  }
}
